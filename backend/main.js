"use strict";
const express = require("express");
const path = require("path");
const rend = require("nunjucks");
const serverless = require("serverless-http");
const AuthController = require("./components/auth/controllers");
const PublicationController = require("./components/publications/controllers");
const userMiddleware = require("./middlewares/auth");
const cors = require("cors");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const app = express();

rend.configure(path.join(__dirname, "views"), {
  autoescape: true,
  watch: true,
  noCache: true,
  express: app,
});

app.set("views", "./views");
app.use(cors());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  return next();
});
app.use(fileUpload());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.get("/api/v1/auth/checkemail/:email", AuthController.checkemail);
app.get("/api/v1/auth/generate-pass/:email", AuthController.generateRandomPass);
app.post("/api/v1/auth/register", AuthController.register);
app.get("/api/v1/auth/superadmin", AuthController.startSuperAdmin);
app.get("/api/v1/auth/getusers", AuthController.getusers);
app.post("/api/v1/auth/login", AuthController.login);
app.get("/api/v1/auth/me", userMiddleware.isAuth, AuthController.me);
app.get("/api/v1/auth/remove-user/:id", AuthController.removeUser);
app.post(
  "/api/v1/auth/update-me",
  userMiddleware.isAuth,
  AuthController.updateme
);
app.get(
  "/api/v1/orders/get-latest-order",
  userMiddleware.isAuth,
  AuthController.getLatestOrder
);
app.post(
  "/api/v1/orders/new-order",
  userMiddleware.isAuth,
  AuthController.newOrder
);
app.get(
  "/api/v1/orders/get-user-orders",
  userMiddleware.isAuth,
  AuthController.getUserOrders
);
app.post("/api/v1/orders/update-order/:id", AuthController.updateOrder);
app.get("/api/v1/orders/get-order/:id", AuthController.getOrderById);
app.get("/api/v1/orders/get-all-orders", AuthController.getAllOrders);
app.post("/api/v1/auth/update-user/:id", AuthController.updateuser);
app.post("/api/v1/auth-activate-account/:id", AuthController.activateUser);
app.post(
  "/api/v1/user/set-avatar",
  userMiddleware.isAuth,
  AuthController.setAvatar
);
app.get("/api/v1/geo/all-cities/:country", AuthController.getallCities);

app.post(
  "/api/v1/publications/new-publication",
  PublicationController.newPublication
);
app.post("/api/v1/publications/add-image/:id", PublicationController.addImage);
app.post(
  "/api/v1/publications/update-publication/:id",
  PublicationController.updatePublication
);
app.get(
  "/api/v1/publications/my-publications",
  PublicationController.myPublications
);
app.post(
  "/api/v1/publications/get-publications/:published",
  PublicationController.getPublications
);
app.get(
  "/api/v1/publications/publication-detail/:id",
  PublicationController.publicationDetail
);
app.get(
  "/api/v1/publications/publication-detail-public/:id",
  PublicationController.publicationDetail
);
app.get(
  "/api/v1/publications/remove-publication/:id",
  PublicationController.removeProducts
);
app.get(
  "/api/v1/publications/remove-image/:id",
  PublicationController.removeImage
);
app.post(
  "/api/v1/publications/categories/new-category",
  PublicationController.newCategory
);
app.post(
  "/api/v1/publications/categories/update-category/:id",
  PublicationController.updateCategory
);
app.post(
  "/api/v1/publications/categories/add-category-image/:id",
  PublicationController.addImageCategory
);
app.get(
  "/api/v1/publications/categories/remove-category/:id",
  PublicationController.removeCategory
);
app.get(
  "/api/v1/publications/categories/get-categories",
  PublicationController.getCategories
);
app.post(
  "/api/v1/orders/new/order",
  userMiddleware.isAuth,
  PublicationController.newOrder
);
app.post("/api/v1/orders/update/order/:id", PublicationController.updateOrder);
app.post("/api/v1/orders/get-orders", PublicationController.updateOrder);
app.get(
  "/api/v1/orders/remove-all-orders",
  PublicationController.removeAllOrders
);

module.exports.handler = serverless(app);
