var Publication = require("./model").Publication;
var Category = require("./model").Category;
var Order = require("./model").Order;
var PublicationImage = require("./model").PublicationImage;
var service = {};
var moment = require("moment");
const connectToDatabase = require("../../config/db-connection");

const AWS = require("aws-sdk");
const spacesEndpoint = new AWS.Endpoint("sfo2.digitaloceanspaces.com");
const ACCESSKEY = "SWKR7TKCWH4UFUWY52N3";
const SECRETKEY = "1FTHAw+hDb2gI8teQqGm4mTM8xviQKDJF5aXFAMC2VM";
var service = require("../../services/auth");
var fs = require("fs");
service.newPublication = function (data) {
  return new Promise(function (resolve, reject) {
    var publicacion = new Publication(data);
    Publication.create(publicacion, function (err, publicationCreated) {
      if (err) {
        return reject(err);
      } else {
        return resolve(publicationCreated);
      }
    });
  });
};

service.newCategory = async (data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Category", Category);
  return new Promise(function (resolve, reject) {
    var category = new Category(data);
    model.create(category, function (err, categoryCreated) {
      if (err) {
        return reject(err);
      } else {
        return resolve(categoryCreated);
      }
    });
  });
};

service.updateCategory = async (categoryId, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Category", Category);
  return new Promise(function (resolve, reject) {
    model.findByIdAndUpdate(categoryId, data, { new: true }, function (
      err,
      categoryEdited
    ) {
      if (err) {
        return reject(err);
      } else {
        return resolve(categoryEdited);
      }
    });
  });
};
service.updatePublication = async (publicationId, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Publication", Publication);
  return new Promise(function (resolve, reject) {
    model.findByIdAndUpdate(publicationId, data, { new: true }, function (
      err,
      publicationEdited
    ) {
      if (err) {
        return reject(err);
      } else {
        return resolve(publicationEdited);
      }
    });
  });
};

service.addImageCategory = async (categoryId, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Category", Category);
  return new Promise(function (resolve, reject) {
    if (!data.files) {
      return reject({ message: "No hay imagenes" });
    } else {
      let file = data.files.image;

      console.log("______-");
      console.log(file);
      let s3bucket = new AWS.S3({
        endpoint: spacesEndpoint,
        accessKeyId: ACCESSKEY,
        secretAccessKey: SECRETKEY,
      });
      let imagekey =
        "categories/" + categoryId + "/" + moment() + "-" + file.name;
      var params = {
        Body: file.data,
        Bucket: "files-adstore",
        ACL: "public-read",
        Key: imagekey,
      };
      console.log("________________________");
      console.log(params);
      s3bucket.putObject(params, function (err, data) {
        if (err) {
          console.log(err, err.stack);
        } else {
          console.log("_____DATA___________________");
          console.log(data);
          let dataImage = {
            url: imagekey,
            publication: categoryId,
          };
          model
            .findByIdAndUpdate(
              categoryId,
              { img: dataImage.url },
              { new: true }
            )
            .exec(function (error, result) {
              if (err) {
                return reject(error);
              } else if (result) {
                return resolve(result);
              }
            });
        }
      });
    }
  });
};

service.addImage = async (publicationId, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Publication", Publication);
  let imageModel = connection.model("Publication", PublicationImage);
  return new Promise(function (resolve, reject) {
    if (!data.files) {
      return reject({ message: "No hay imagenes" });
    } else {
      let file = data.files.image;

      console.log("______-");
      console.log(file);
      let s3bucket = new AWS.S3({
        endpoint: spacesEndpoint,
        accessKeyId: ACCESSKEY,
        secretAccessKey: SECRETKEY,
      });
      let imagekey =
        "publications/" + publicationId + "/" + moment() + "-" + file.name;
      var params = {
        Body: file.data,
        Bucket: "files-adstore",
        ACL: "public-read",
        Key: imagekey,
      };
      console.log("________________________");
      console.log(params);
      s3bucket.putObject(params, function (err, data) {
        if (err) {
          console.log(err, err.stack);
        } else {
          console.log("_____DATA___________________");
          console.log(data);
          let dataImage = {
            url: imagekey,
            publication: publicationId,
          };
          imageModel.create(dataImage, function (err, imageAdded) {
            if (err) {
              return reject(err);
            } else {
              model
                .findByIdAndUpdate(
                  publicationId,
                  { $push: { images: imageAdded._id } },
                  { new: true }
                )
                .populate({ path: "images", model: imageModel })
                .exec(function (error, result) {
                  if (err) {
                    return reject(error);
                  } else if (result) {
                    return resolve(result);
                  }
                });
            }
          });
        }
      });
    }
  });
};

service.myPublications = async (userid) => {
  let connection = await connectToDatabase();
  let model = connection.model("Publication", Publication);
  let imageModel = connection.model("PublicationImage", PublicationImage);

  return new Promise(function (resolve, reject) {
    model
      .find({ user: userid })
      .populate({ path: "images", model: imageModel })
      .exec(function (err, publications) {
        if (err) {
          return reject(err);
        } else {
          return resolve(publications);
        }
      });
  });
};
service.getPublications = async (data, published) => {
  let connection = await connectToDatabase();
  let model = connection.model("Publication", Publication);
  let publicationImage = connection.model("PublicationImage", PublicationImage);
  return new Promise(function (resolve, reject) {
    if (published === "true") {
      data.isactive = true;
    }
    model.count(data, function (err, count) {
      if (err) {
        return reject(err);
      } else {
        console.log("___________DAT_");
        console.log(count);
        console.log(data);
        model
          .find(data)
          .populate({ path: "images", model: publicationImage })
          .exec(function (err, publications) {
            if (err) {
              return reject(err);
            } else {
              // console.log(publications)
              return resolve({ publications, count });
            }
          });
      }
    });
  });
};

service.getCategories = async () => {
  let connection = await connectToDatabase();
  let model = connection.model("Category", Category);
  return new Promise(function (resolve, reject) {
    model.find({}).exec(function (err, categories) {
      if (err) {
        return reject(err);
      } else {
        return resolve(categories);
      }
    });
  });
};

service.publicationDetail = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("Publication", Publication);
  let publicationImage = connection.model("PublicationImage", PublicationImage);
  return new Promise(function (resolve, reject) {
    model
      .findById(id)
      .populate({ path: "images", model: publicationImage })
      .exec(function (err, publications) {
        if (err) {
          return reject(err);
        } else {
          return resolve(publications);
        }
      });
  });
};

service.removeImage = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("PublicationImage", PublicationImage);
  return new Promise(function (resolve, reject) {
    model.findById(id).exec(function (err, list) {
      if (err) {
        return reject(err);
      } else {
        if (!list) {
          return resolve({ code: "404", message: "Recurso no encontrado" });
        } else {
          fs.stat("./uploads/" + id + ".jpg", function (err, stats) {
            if (err) {
              return console.error(err);
            } else {
              fs.unlink("./uploads/" + id + ".jpg", function (err) {
                if (err) {
                  return reject(err);
                } else {
                  model.remove({ _id: id }, function (err) {
                    if (!err) {
                      return resolve({
                        code: "100",
                        message: "Recurso eliminado",
                      });
                    } else {
                      return reject(err);
                    }
                  });
                }
              });
            }
          });
        }
      }
    });
  });
};

service.removeProducts = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("Publication", Publication);
  return new Promise(function (resolve, reject) {
    model.findByIdAndRemove(id, function (err, removed) {
      if (err) {
        return reject(error);
      } else {
        return resolve(removed);
      }
    });
  });
};
service.removeImage = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("PublicationImage", PublicationImage);
  return new Promise(function (resolve, reject) {
    model.findByIdAndRemove(id, function (err, removed) {
      if (err) {
        return reject(error);
      } else {
        return resolve(removed);
      }
    });
  });
};
service.removeCategory = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("Category", Category);
  return new Promise(function (resolve, reject) {
    model.findByIdAndRemove(id, function (err, removed) {
      if (err) {
        return reject(error);
      } else {
        return resolve(removed);
      }
    });
  });
};
service.newOrder = async (userId, order) => {
  let connection = await connectToDatabase();
  let model = connection.model("Order", Order);
  return new Promise(function (resolve, reject) {
    var data = new Order({
      user: userId,
      order_date: moment(),
      products: order,
    });
    model.create(data, function (err, created) {
      if (err) {
        return reject(error);
      } else {
        return resolve(created);
      }
    });
  });
};

service.getOrders = async (data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Order", Order);
  let userModel = connection.model("User", User);

  return new Promise(function (resolve, reject) {
    model.count(data, function (err, count) {
      if (err) {
        return reject(err);
      } else {
        model
          .find(data)
          .populate({ path: "user", model: userModel })
          .exec(function (err, orders) {
            if (err) {
              return reject(err);
            } else {
              return resolve({ orders, count });
            }
          });
      }
    });
  });
};

service.updateOrder = async (orderId, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Order", Order);
  return new Promise(function (resolve, reject) {
    model.findByIdAndUpdate(orderId, data, { new: true }, function (
      err,
      created
    ) {
      if (err) {
        return reject(error);
      } else {
        return resolve(created);
      }
    });
  });
};

service.removeAllOrders = async () => {
  let connection = await connectToDatabase();
  let model = connection.model("Order", Order);
  return new Promise(function (resolve, reject) {
    model.dropCollection(function (err, created) {
      if (err) {
        return reject(error);
      } else {
        return resolve(created);
      }
    });
  });
};

module.exports = service;
