"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var PublicationImageSchema = Schema({
  url: { type: String },
  publication: { type: Schema.Types.ObjectId, ref: "Publication" },
});

module.exports.PublicationImage = PublicationImageSchema;

var CategorySchema = Schema({
  img: { type: String },
  name: { type: String },
});

module.exports.Category = CategorySchema;

var PublicationSchema = Schema({
  title: { type: String },
  price: { type: Number },
  units_pack: { type: Number },
  type_unit: { type: String },
  ismultiple: { type: Boolean },
  category: { type: Schema.Types.ObjectId, ref: "Category" },
  images: [{ type: Schema.Types.ObjectId, ref: "PublicationImage" }],
  sizes: [],
  skills: {
    type: Array,
    default: [
      {
        name: "Adicional",
        value: 0,
      },
    ],
  },
  description: { type: String },
  creased: { type: String },
  material: { type: String },
  tunning: { type: Boolean },
  plasticized: { type: String },
  diler_logo: { type: Boolean },
  min_unites: { type: Number },
  max_unites: { type: Number },
  min_delivery: { type: Number },
  max_delivery: { type: Number },
  delivery: { type: Number, default: 0 },
  isactive: { type: Boolean },
});

module.exports.Publication = PublicationSchema;

var OrderSchema = Schema({
  user: { type: Schema.Types.ObjectId, ref: "User" },
  products: [],
  order_date: { type: Date },
  order_state: { type: Number, default: 0 },
});

module.exports.Order = OrderSchema;
