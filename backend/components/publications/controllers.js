var Services = require('./services');


exports.newPublication = function(req,res,next){
	var params = req.body;
	Services.newPublication(params)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};

exports.addImage = function(req,res,next){
	// var id = req.user.sub;
	var publicationId = req.params.id;
	var data = req;
	data.publication = publicationId;
	Services.addImage(publicationId ,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.updatePublication = function(req,res,next){
	var params = req.body;
	var publicationId = req.params.id;
	Services.updatePublication(publicationId,params)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.myPublications = function(req,res,next){
	var id = req.user.sub;
	Services.myPublications(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getPublications = function(req,res,next){
	var data = req.body;
	var published = req.params.published;
	Services.getPublications(data,published)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}

exports.publicationDetail = function(req,res,next){
	var id = req.params.id;
	Services.publicationDetail(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};


exports.newCategory = function(req,res,next){
	var params = req.body;
	Services.newCategory(params)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.updateCategory = function(req,res,next){
	var params = req.body;
	var categoryId = req.params.id;
	Services.updateCategory(categoryId,params)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.addImageCategory = function(req,res,next){
	// var id = req.user.sub;
	var categoryId = req.params.id;
	var data = req;
	data.publication = categoryId;
	Services.addImageCategory(categoryId ,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.removeCategory = function(req,res,next){
	// var id = req.user.sub;
	var categoryId = req.params.id;
	Services.removeCategory(categoryId)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getCategories = function(req,res,next){
	Services.getCategories()
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}
exports.removeProducts = function(req,res,next){
	var id = req.params.id;
	Services.removeProducts(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.removeImage = function(req,res,next){
	var id = req.params.id;
	Services.removeImage(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};

exports.newOrder = function(req,res,next){
	var userId = req.user.sub;
	var order = req.body;
	Services.newOrder(userId, order)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}
exports.getOrders = function(req,res,next){
	var data = req.body;
	Services.getOrders(data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}
exports.updateOrder = function(req,res,next){
	var data = req.body;
	var orderId = req.params.id;
	Services.updateOrder(orderId, data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}
exports.removeAllOrders = function(req,res,next){
	Services.removeAllOrders()
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}