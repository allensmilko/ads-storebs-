var User = require("./model").User;
var Order = require("./model").Orders;
var service = {};
var generator = require("generate-password");
var moment = require("moment");
var bcrypt = require("bcrypt-nodejs");
var service = require("../../services/auth");
var moment = require("moment");
const AWS = require("aws-sdk");
const connectToDatabase = require("../../config/db-connection");
const spacesEndpoint = new AWS.Endpoint("sfo2.digitaloceanspaces.com");
const ACCESSKEY = "SWKR7TKCWH4UFUWY52N3";
const SECRETKEY = "1FTHAw+hDb2gI8teQqGm4mTM8xviQKDJF5aXFAMC2VM";

service.startSuperAdmin = async (data) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    data.ultima_modificacion = moment().format();
    data.fecha_creacion = moment().format();
    data.ultima_recarga = moment().format();
    model.findOne({ username: data.username }, function (err, result) {
      if (err) {
        console.log("Error 1 -------------");
        return reject(err);
      } else if (result) {
        console.log("Error 2 -------------");
        return resolve(result);
      } else {
        console.log("No existe -------------");

        User.create(data, function (err, UsuarioCreado) {
          if (err) {
            console.log("Error 3 -------------");
            return reject(err);
          } else {
            console.log("Error 4 -------------");
            return resolve(UsuarioCreado);
          }
        });
      }
    });
  });
};

service.getusers = async (data) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model.find(data, function (err, result) {
      if (err) {
        console.log("Error 1 -------------");
        return reject(err);
      } else {
        return resolve(result);
      }
    });
  });
};

service.checkemail = async (data) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model.findOne({ email: data.toLowerCase() }, function (err, user) {
      // Comprobar si hay errores
      // Si el User existe o no
      // Y si la contraseña es correcta
      if (err) {
        return reject({
          message: "Ocurrio un error al hacer la peticion",
          error: err,
        });
      } else {
        if (user) {
          if (!user.isactive) {
            return resolve({ code: 103, message: "La cuenta no esta activa" });
          } else {
            if (user.is_social) {
              return resolve({ code: 104, is_social: user.is_social });
            } else {
              return resolve({ code: 100, is_social: user.is_social });
            }
          }
        } else {
          return reject({ code: 101, message: "El usuaio no existe" });
        }
      }
    });
  });
};
service.register = async (params) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);

  return new Promise(function (resolve, reject) {
    var user = new model(params);
    if (!params.avatar) {
      user.avatar = "-";
    } else {
      user.avatar = params.avatar;
    }
    bcrypt.hash(params.password, null, null, function (err, hash) {
      if (err) {
        console.log(err);
      } else {
        user.password = hash;
      }
    });

    model.findOne({ email: params.email }, (err, userfinded) => {
      if (userfinded) {
        return resolve({ code: 404, message: "Este User ya existe" });
      } else {
        user.save(function (err, usercreated) {
          if (err) {
            return reject({
              code: 501,
              data: { message: "Error al crear el usuario", err: err },
            });
          } else {
            var token = service.createToken(usercreated);

            return resolve({
              code: 100,
              message: "User creado con exito!",
              token: token,
              user: user,
            });
          }
        });
      }
    });
  });
};
service.login = async (data) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model.findOne({ email: data.email.toLowerCase() }, function (err, user) {
      // Comprobar si hay errores
      // Si el User existe o no
      // Y si la contraseña es correcta
      if (err) {
        return reject({
          message: "Petición denegada, correo o contraseña invalidos",
        });
      } else {
        if (user) {
          console.log(data.password);
          console.log(user.password);
          bcrypt.compare(data.password, user.password, (err, check) => {
            if (check) {
              if (!user.isactive) {
                return reject({
                  code: 401,
                  message: "El usuario no esta activado",
                });
              } else {
                return resolve({
                  code: 100,
                  rol: user.rol,
                  token: service.createToken(user),
                });
              }
            } else {
              console.log(err);
              return reject({
                code: 404,
                message: "El usuario no pudo logearse correctamente",
              });
            }
          });
        } else {
          return reject({
            code: 404,
            message: "El usuario no pudo logearse correctamente",
          });
        }
      }
    });
  });
};

service.setAvatar = async (id, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    if (!data.files) {
      return reject({ message: "No hay imagenes" });
    } else {
      let file = data.files.image;
      let s3bucket = new AWS.S3({
        endpoint: spacesEndpoint,
        accessKeyId: ACCESSKEY,
        secretAccessKey: SECRETKEY,
      });
      let imagekey = "avatars/" + id + "/" + moment() + "-" + file.name;
      var params = {
        Body: file.data,
        Bucket: "files-adstore",
        ACL: "public-read",
        Key: imagekey,
      };
      console.log("________________________");
      console.log(params);
      s3bucket.putObject(params, function (err, data) {
        if (err) {
          console.log(err, err.stack);
        } else {
          var localdata = {
            avatar: imagekey,
          };
          model.findByIdAndUpdate(id, localdata, { new: true }, function (
            error,
            result
          ) {
            if (err) {
              return reject(error);
            } else {
              console.log(result);
              return resolve(result);
            }
          });
        }
      });
    }
  });
};

service.me = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model.findById(id, function (err, list) {
      if (err) {
        return reject(err);
      } else {
        if (list) {
          return resolve(list);
        } else {
          return reject({ message: "el usuario no existe", code: 404 });
        }
      }
    });
  });
};
service.removeUser = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model.findByIdAndRemove(id, function (err, list) {
      if (err) {
        return reject(err);
      } else {
        if (list) {
          return resolve(list);
        } else {
          return reject({ message: "el usuario no existe", code: 404 });
        }
      }
    });
  });
};
service.updateme = async (id, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model.findByIdAndUpdate(id, data, { new: true }, function (err, list) {
      if (err) {
        return reject(err);
      } else {
        return resolve(list);
      }
    });
  });
};

service.createOrder = async (id, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  let orderModel = connection.model("Order", Order);

  return new Promise(function (resolve, reject) {
    console.log("______________");
    var ord = new orderModel(data);
    ord.order_date = moment().format("YYYY-MM-DD");
    ord.created_at = moment().unix();
    ord.buyer = id;
    ord.save(function (err, created) {
      if (err) {
        return reject(err);
      } else {
        console.log(created);
        model.findByIdAndUpdate(
          id,
          { $push: { orders: created._id } },
          { new: true },
          function (err, list) {
            if (err) {
              return reject(err);
            } else {
              return resolve({ order: created, user: list });
            }
          }
        );
      }
    });
  });
};

service.getUserOrders = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  let userOrders = connection.model("Order", Order);
  return new Promise(function (resolve, reject) {
    model
      .findById(id)
      .populate({ path: "orders", model: userOrders })
      .exec(function (err, user) {
        if (err) {
          return reject(err);
        } else {
          // console.log(user)
          return resolve(user);
        }
      });
  });
};
service.getAllOrders = async (id) => {
  let connection = await connectToDatabase();
  let model = connection.model("Order", Order);
  let userModel = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model
      .find({})
      .populate({ path: "buyer", model: userModel })
      .sort({ order: -1 })
      .exec(function (err, orders) {
        if (err) {
          return reject(err);
        } else {
          // console.log(user)
          return resolve(orders);
        }
      });
  });
};

service.getLatestOrder = async () => {
  let connection = await connectToDatabase();
  let model = connection.model("Order", Order);
  return new Promise(function (resolve, reject) {
    model.findOne({}, {}, { sort: { created_at: -1 } }, function (err, order) {
      console.log("___________-------____________________");
      console.log(order);
      if (err) {
        return reject(err);
      } else {
        if (order === null) {
          return resolve({ order: 0 });
        } else {
          return resolve(order);
        }
        // console.log(user)
      }
    });
  });
};

service.updateOrderS = async (id, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Order", Order);
  return new Promise(function (resolve, reject) {
    model
      .findByIdAndUpdate(id, data, { new: true })
      .exec(function (err, orders) {
        if (err) {
          return reject(err);
        } else {
          // console.log(user)
          return resolve(orders);
        }
      });
  });
};

service.getOrderById = async (id, data) => {
  let connection = await connectToDatabase();
  let model = connection.model("Order", Order);
  let userModel = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model
      .findById(id)
      .populate({ path: "buyer", model: userModel })
      .exec(function (err, order) {
        if (err) {
          return reject(err);
        } else {
          // console.log(user)
          return resolve(order);
        }
      });
  });
};

service.generateRandomPass = async (email) => {
  let connection = await connectToDatabase();
  let model = connection.model("User", User);
  return new Promise(function (resolve, reject) {
    model.findOne({ email: email }, (err, userfinded) => {
      if (userfinded) {
        var password = generator.generate({
          length: 6,
          numbers: true,
        });
        var finalePass = {
          password: "",
        };
        bcrypt.hash(password, null, null, function (err, hash) {
          if (err) {
            return resolve({
              code: 500,
              message: "Error al generar la cntraseña",
            });
          } else {
            finalePass.password = hash;
            model.findByIdAndUpdate(
              userfinded._id,
              finalePass,
              { new: true },
              function (err, list) {
                if (err) {
                  return reject(err);
                } else {
                  return resolve({ user: list, newpass: password });
                }
              }
            );
          }
        });
      } else {
        return resolve({ code: 404, message: "Este usuario no existe" });
      }
    });
  });
};
module.exports = service;
