const Services = require("./services");
const Handlebars = require("handlebars");
const fs = require("fs");
const mailService = require("../../emails/difusion");
const EmailTemplate = require("email-templates");
const path = require("path");

exports.startSuperAdmin = function (req, res, next) {
  var usuario = {
    username: "Super Admin",
    email: "administrator@dstorebs.com",
    password: "D3lunoalc3ro",
    rol: 1,
    isfirsttime: false,
    isactive: true,
  };
  Services.register(usuario)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      console.error(err);
    });
};

exports.register = function (req, res, next) {
  const { body } = req;

  Services.register(body)
    .then(function (user) {
      if (user.code == 404) {
        res.send(user);
      } else {
        console.log(user);
        mailService
          .sendDifussion(
            { email: user.user.email, username: user.user.username },
            "Solicitud de registro",
            "ads.tore.bridgestone@gmail.com",
            "request"
          )
          .then((sended) => {
            console.log(sended);
            res.status(200).send({
              message: user.message,
              token: user.token,
              code: user.code,
            });
          })
          .catch((errorSend) => {
            console.log(errorSend);
            res.status(500).send(errorSend);
          });
      }
    })
    .catch(function (err) {
      res.status(500).send(err);
    });
};
exports.login = function (req, res, next) {
  Services.login(req.body)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};

exports.getusers = function (req, res, next) {
  const { query = {} } = req;
  Services.getusers(query)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};

// I changed the emails from what's in the tutorial because people kept using
// info@geeklaunch.net and sending me their test emails. :P Lesson learned. :)
//
// So yeah, change the emails below from 'example@example.tld' to YOUR email,
// please.
//
// Thank you!

function sendEmail(obj) {
  return transporter.sendMail(obj);
}

function loadTemplate(templateName, contexts) {
  let template = new EmailTemplate(
    path.join(__dirname, "../../templates", templateName)
  );
  return Promise.all(
    contexts.map((context) => {
      return new Promise((resolve, reject) => {
        template.render(context, (err, result) => {
          if (err) reject(err);
          else
            resolve({
              email: result,
              context,
            });
        });
      });
    })
  );
}

exports.setAvatar = function (req, res, next) {
  var id = req.user.sub;
  console.log(id);
  var data = req;
  Services.setAvatar(id, data)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.me = function (req, res, next) {
  var id = req.user.sub;
  Services.me(id)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.removeUser = function (req, res, next) {
  var id = req.params.id;
  Services.removeUser(id)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.updateme = function (req, res, next) {
  var id = req.user.sub;
  var data = req.body;
  Services.updateme(id, data)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.newOrder = function (req, res, next) {
  var id = req.user.sub;
  var data = req.body;
  Services.createOrder(id, data)
    .then(function (resultNewOrder) {
      mailService
        .sendDifussion(
          {
            order_date: resultNewOrder.order.order_date,
            products: resultNewOrder.order.products,
            ordersubtotal: resultNewOrder.order.ordersubtotal,
            ordertotal: resultNewOrder.order.ordertotal,
            delivering: resultNewOrder.order.delivering,
          },
          "¡Recibimos tu pedido!",
          resultNewOrder.user.email,
          "new-order"
        )
        .then((sended) => {
          console.log(sended);
          res.status(200).send(resultNewOrder.order);
        })
        .catch((errorSend) => {
          console.log(errorSend);
          res.status(500).send(errorSend);
        });
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.getUserOrders = function (req, res, next) {
  var id = req.user.sub;
  Services.getUserOrders(id)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.getAllOrders = function (req, res, next) {
  Services.getAllOrders()
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.updateOrder = function (req, res, next) {
  var orderid = req.params.id;
  var order = req.body;
  Services.updateOrderS(orderid, order)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.updateuser = function (req, res, next) {
  var id = req.params.id;
  var data = req.body;
  Services.updateme(id, data)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};

exports.activateUser = function (req, res, next) {
  var id = req.params.id;
  var data = req.body;
  Services.updateme(id, data)
    .then(function (result) {
      if (data.isactive) {
        var mailOptions = {
          from: '"ADSTORE" <ads.tore.bridgestone@gmail.com>', // sender address
          to: result.email, // list of receivers
          subject: "Activación de cuenta!", // Subject line
          text:
            "Hola " +
            result.username +
            " se activo tu cuenta de forma exitosa!,  ya puedes iniciar sesión con la cuenta que te registraste.", // plain text body
          html:
            "<p>Hola <b>" +
            result.username +
            '</b> se activo tu cuenta de forma exitosa! <b></p><p> ya puedes iniciar sesión con la cuenta que te registraste oprimiendo click <a href="http://adstorebs.com/auth" target="_blak">aquí</a> o en el siguiente link <a href="http://adstorebs.com/auth" target="_blak">http://adstorebs.com/auth</a></p>', // html body
        };
      } else {
        var mailOptions = {
          from: '"ADSTORE" <ads.tore.bridgestone@gmail.com>', // sender address
          to: result.email, // list of receivers
          subject: "Cuenta desactivada", // Subject line
          text:
            "Hola " +
            result.username +
            " tu cuenta ha sido desactivada por el administrador,  ponte en contácto con el equipo de ADSTORE para solicitar una reactivación.", // plain text body
          html:
            "<p>Hola <b>" +
            result.username +
            "</b> tu cuenta ha sido desactivada por el administrador <b></p><p> ponte en contácto con el equipo de ADSTORE para solicitar una reactivación</p>", // html body
        };
      }
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return reject(error);
        } else {
          res.status(200).send(result);
        }
      });
    })
    .catch(function (err) {
      res.send(err);
    });
};

exports.generateRandomPass = function (req, res, next) {
  var id = req.params.email;
  Services.generateRandomPass(id)
    .then(function (result) {
      console.log(result);
      if (result.code === 404) {
        res.status(404).send({ message: "El usuario nno existe", code: 404 });
      } else {
        var mailOptions = {
          from: '"ADSTORE" <ads.tore.bridgestone@gmail.com>', // sender address
          to: result.user.email, // list of receivers
          subject: "cambio de contraseña!", // Subject line
          text:
            "Hola " +
            result.user.username +
            " se realizao un cambio de contraseña , esta es tu nueva contraseña:" +
            result.newpass, // plain text body
          html:
            "<p>Hola <b>" +
            result.user.username +
            "</b>se realizao un cambio de contraseña , esta es tu nueva contraseña:  </p><p><b>" +
            result.newpass +
            "</b></p>", // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            res.status(500).send(error);
          } else {
            res.status(200).send(result);
          }
        });
      }
    })
    .catch(function (err) {
      res.send(err);
    });
};

exports.checkemail = function (req, res, next) {
  var email = req.params.email;
  Services.checkemail(email)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};
exports.getallCities = function (req, res, next) {
  var country = req.params.country;
  var list = require("../../static/geo/" + country + ".json");
  res.status(200).send(list);
};
exports.getLatestOrder = function (req, res, next) {
  Services.getLatestOrder()
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
};

exports.getOrderById = function (req, res, next) {
  const { id } = req.params;
  Services.getOrderById(id)
    .then(function (result) {
      // res.send(result);
      let source = fs.readFileSync(
        path.join(__dirname, "../../views/orders/order-detail.hbs"),
        "utf8"
      );

      let template = Handlebars.compile(source);

      let orderMailOptions = {
        from: '"ADSTORE" <ads.tore.bridgestone@gmail.com>', // sender address
        to: "ads.tore.bridgestone@gmail.com", // list of receivers
        subject: "Solicitud de registro",
        text:
          "Hola  se activo tu cuenta de forma exitosa!,  ya puedes iniciar sesión con la cuenta que te registraste.", // plain text body
        html: template(result),
        // template: 'orders/order-detail',
        // context: result
      };

      // const email = new EmailTemplate({
      // 	template: 'orders/order-detail',
      // 	message: {
      // 		from: '"ADSTORE" <ads.tore.bridgestone@gmail.com>', // sender address
      // 		to: 'ads.tore.bridgestone@gmail.com', // list of receivers
      // 		subject: 'Solicitud de registro',
      // 	},
      // 	locals:result,
      // 	transport: {
      // 		jsonTransport: true,
      // 		to: 'ads.tore.bridgestone@gmail.com'
      // 	}
      // });
      // Send Mail with Nodemailer

      //   transporter.sendMail(email

      transporter.sendMail(orderMailOptions, (error, sended) => {
        if (error) {
          console.log(error);

          res.status(403).send({ MENSAJE: "ERROR", error });
        } else {
          console.log(result);
          // res.status(200).send({"MENSAJE":"SUCCESS",sended});
          res.render("orders/order-detail", result);
        }
      });
    })
    .catch(function (err) {
      console.log(err);
      res.status(404).send(err);
    });
};
