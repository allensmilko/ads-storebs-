"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var AutoIncrement = require("mongoose-sequence")(mongoose);

var CountrySchema = Schema({
  Country_name: { type: String, required: true },
});

module.exports.Country = CountrySchema;

var StateSchema = Schema({
  state_name: { type: String, required: true },
});

module.exports.State = StateSchema;

var CitySchema = Schema({
  city_name: { type: String, required: true },
});
module.exports.City = CitySchema;

var UserSchema = Schema({
  username: { type: String, required: true },
  personal_names: { type: String },
  personal_lastnames: { type: String },
  corporative_email: { type: String },
  personal_email: { type: String },
  personal_id: { type: String },
  birdth_date: { type: Date, required: false },
  email: { type: String, default: true },
  password: { type: String, default: true },
  avatar: { type: String, required: false },
  isactive: { type: Boolean, default: false },
  isfirsttime: { type: Boolean, default: true },
  newsletter: { type: Boolean, default: false },
  termscheck: { type: Boolean, default: false },
  coins: { type: Number, default: 0 },
  country: { type: String },
  phones: [{ type: Number, default: 0 }],
  register_date: { type: Date },
  is_social: { type: Boolean },
  rol: { type: Number },
  social_name: { type: String, required: false },
  nit: { type: String, required: false },
  city: { type: String, required: false },
  address: { type: String, required: false },
  description: { type: String, required: false },
  web: { type: String, required: false },
  orders: [{ type: mongoose.Schema.Types.ObjectId, ref: "Orders" }],
});

module.exports.User = UserSchema;

var OrdersSchema = Schema({
  ordertotal: { type: Number, default: 0 },
  ordersubtotal: { type: Number, default: 0 },
  delivery: { type: Number, default: 0 },
  status: { type: Number, default: 0 },
  order: { type: Number, default: 0 },
  products: [],
  order_date: { type: Date },
  created_at: { type: Date },
  buyer: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
});

// OrdersSchema.plugin(AutoIncrement, { inc_field: "order" });
module.exports.Orders = OrdersSchema;
