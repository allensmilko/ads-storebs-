const { RECONECTTIRES, RECONNECTINTERVAL, SOCKETTIMEOUT, CONNECTTIMEOUT, NEWURLPARSER, MONGO_DB } = process.env, 
    options = {
        reconnectTries: RECONECTTIRES,
        reconnectInterval: RECONNECTINTERVAL,
        socketTimeoutMS: SOCKETTIMEOUT,
        connectTimeoutMS: CONNECTTIMEOUT,
        useNewUrlParser: NEWURLPARSER,
        ssl: true
    },
    mongoose = require('mongoose');
    let cachedDb = null;
 
module.exports = connectToDatabase = async() => {
    if (cachedDb == null) {
        console.log("No Cacheada");
        console.log(cachedDb);
       return cachedDb = await mongoose.createConnection(MONGO_DB, {
            // Buffering means mongoose will queue up operations if it gets
            // disconnected from MongoDB and send them when it reconnects.
            // With serverless, better to fail fast if not connected.
            bufferCommands: false, // Disable mongoose buffering
            bufferMaxEntries: 0 // and MongoDB driver buffering
          });
    }else{
        console.log("Cacheada");
        console.log(cachedDb);
        return cachedDb;
    }
}