import { Component } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'adstorebs-client';
  currout = '';
  token = '';
  constructor(private router: Router, private auth: AuthService) {
    router.events.subscribe( (event: Event) => {

      if (event instanceof NavigationStart) {
          // Show loading indicator
          
          console.log('preparing to load...')
          let node = document.createElement('script');
          node.src = 'https://static.zdassets.com/ekr/snippet.js?key=b0de06f6-464d-40a3-8494-c81931b32005';
          node.type = 'text/javascript';
          node.async = true;
          node.setAttribute("id", "ze-snippet");
          node.charset = 'utf-8';
          document.getElementsByTagName('head')[0].appendChild(node);
      }

      if (event instanceof NavigationEnd) {
          // Hide loading indicator
          this.token = this.auth.getToken();
          console.log('___________');
          console.log(this.token);
          this.currout = window.location.pathname;
          if ((this.currout !== '/auth' && this.currout !== '/terminos-y-condiciones') && this.token == null) {
            this.router.navigate(['/auth']);
          }
      }

      if (event instanceof NavigationError) {
          // Hide loading indicator

          // Present error to user
          console.log(event.error);
      }
    });
  }
}
