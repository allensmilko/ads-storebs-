import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass']
})
export class ProductsComponent implements OnInit {
  products: any = {
    publications: [],
    count: 0
  };
  response: any = {};
  isadmin = true;
  constructor(private serviceProduct: ProductsService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.serviceProduct.getPublications({}, false)
    .subscribe(
      data => { console.log(data); this.products = data; },
      err => console.error(err),
      () => console.log('done loading foods')
    );

  }
  newPublication() {
    const data = {
      isactive: false
    };
    const token = '';
    this.serviceProduct.newPublication(token, data)
    .subscribe(
      resp => {
        console.log(resp);
        this.response = resp;
        window.location.href = '/admin/gestion-publicacion/' + this.response._id;
      },
      err => console.error(err),
      () => console.log('done')
    );
  }


}
