import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../products.service';
import { GlobalVariable } from '../../../config';
declare var UIkit: any;

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categories: any = [];
  isadmin = true;
  globaluri = GlobalVariable.IMAGESURI;
  category = {
    name: ''
  };
  curCat = {
    _id: ''
  }
  constructor(private serviceRpoducts: ProductsService) { }

  ngOnInit() {
    this.getCategories();
  }
  newCategory() {
    this.serviceRpoducts.newCategory('token', this.category)
    .subscribe(
      data => { console.log(data); this.getCategories(); },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  getCategories() {
    this.serviceRpoducts.getCategories()
    .subscribe(
      data => { console.log(data); this.categories = data; },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  onFileChange(fileInput: any, categoryId) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      console.log(fileInput.target.files[0]);
      this.serviceRpoducts.addImageToCategory('', categoryId, fileInput.target.files[0])
      .subscribe(
        // tslint:disable-next-line:max-line-length
        data => { console.log(data); this.getCategories(); },
        err => console.error(err),
        () => console.log('done loading image')
      );
  }
}
selectForRemove(category) {
  this.curCat = category;
  UIkit.modal('#modal-remove').show();
}
removeCategory() {
  this.serviceRpoducts.removeCategory(this.curCat._id)
      .subscribe(
        // tslint:disable-next-line:max-line-length
        data => { 
          console.log(data);
          this.getCategories();
          UIkit.modal('#modal-remove').hide(); },
        err => console.error(err),
        () => console.log('done loading image')
      );
}
}

