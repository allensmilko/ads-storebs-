import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../products.service';
import { ActivatedRoute } from '@angular/router';
import { GlobalVariable } from '../../../config';

const token = localStorage.getItem('adstore-auth-token');
declare var UIkit: any;

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  publication: any = {
    skills: [
    ],
    min_unites: 0,
    units_pack: 0
  };
  categories: any = [];
  provitional: any = {
    images: []
  };
  publicationId: string;
  lat: any = 0;
  lng: any = 0;
  avisomodal = '';
  globaluri = GlobalVariable.IMAGESURI;


  constructor(private servicePublication: ProductsService, private route: ActivatedRoute) {
    this.publicationId = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.getPublicationData();
    this.getCategories();
    // this.getCurrentLocation();
  }

  // getCurrentLocation() {
  //   if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition((position) => {
  //       this.publication.location = {
  //         type: 'Point',
  //         coordinates: [ position.coords.longitude, position.coords.latitude ]
  //       } ;
  //     });
  //   } else {
  //     alert('Tu navegador no tiene activada la geolocalización o no la soporta');
  //   }
  // }
  getPublicationData() {
    this.servicePublication.getPublicationIdPrivated(token, this.publicationId)
    .subscribe(
      resp => {
        console.log(resp);
        this.publication = resp;
        console.log(this.publication);
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
  getCategories() {
    this.servicePublication.getCategories()
    .subscribe(
            data => { console.log(data); this.categories = data; },
            err => console.error(err),
            () => console.log('done loading foods')
          );
  }
  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      console.log(fileInput.target.files[0]);
      this.servicePublication.addImageToPublication(token, this.publicationId, fileInput.target.files[0])
      .subscribe(
        // tslint:disable-next-line:max-line-length
        data => { console.log(data); this.provitional = data; this.publication.images = this.provitional.images ; this.avisomodal = 'Imagen agregada con exito!!'; UIkit.modal('#modal-aviso').show(); },
        err => console.error(err),
        () => console.log('done loading image')
      );
  }
}
newSkill(event) {
  event.preventDefault();
  this.publication.skills.push({name: 'caracteristica', value: 0 });
}
  onSubmit() {
    console.log(this.publication);
    this.publication.min_unites = this.publication.units_pack;
    this.servicePublication.updatePublication(token, this.publicationId, this.publication).subscribe(
      // tslint:disable-next-line:max-line-length
      data => { console.log(data); this.getPublicationData(); this.avisomodal = 'Actualización realizada con exito!'; UIkit.modal('#modal-aviso').show(); },
      err => console.error(err),
      () => console.log('done loading image')
    );
  }
  reoveImage(imgid) {
    console.log(this.publication);
    this.servicePublication.removeImage(imgid).subscribe(
      // tslint:disable-next-line:max-line-length
      data => { console.log(data); this.getPublicationData(); this.avisomodal = 'Actualización realizada con exito!'; UIkit.modal('#modal-aviso').show(); },
      err => console.error(err),
      () => console.log('done loading image')
    );
  }

}





