import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth.service';
import { GlobalVariable } from '../../../config';
declare var UIkit: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: any = [];curstatus
  globalUri = GlobalVariable.IMAGESURI;
  curUser = {
    _id: ''
  };
  roles = [{
    rol:"Super admin",
    value: 1
  },{
    rol: "Usuario",
    value: 2
  }];
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.getUsers();
  }
  getUsers() {
    this.auth.getusers()
    .subscribe(
      data => { console.log(data); this.users = data; },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  activateuser(user) {
    user.isactive = true;
    this.auth.activateAccount(user, user._id)
    .subscribe(
      data => { console.log(data); UIkit.modal.alert('Actualización realizada con exito!');this.getUsers(); },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  inactive(user) {
    user.isactive = false;
    this.auth.activateAccount(user, user._id)
    .subscribe(
      data => { console.log(data); UIkit.modal.alert('Actualización realizada con exito!'); this.getUsers(); },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  upDatUs(user) {
    this.auth.updateuser(user, user._id)
    .subscribe(
      data => { console.log(data); UIkit.modal.alert('Actualización realizada con exito!'); this.getUsers(); },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  prepareToRemeove(user) {
    this.curUser = user;
    UIkit.modal('#modal-remove').show();
  }
  removeuser() {
    UIkit.modal('#modal-remove').hide();
    this.auth.removeuser(this.curUser._id)
    .subscribe(
      data => { console.log(data); UIkit.modal.alert('Actualización realizada con exito!'); this.getUsers(); },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
}
