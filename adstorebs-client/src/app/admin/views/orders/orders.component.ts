import { Component, OnInit } from '@angular/core';
import { CartService } from '../../../cart.service';
declare var UIkit: any;

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  myOrders: any = [];
  curOrder = {
    status: 0,
    buyer: {
      phones: []
    },
    products:[],
    ordertotal: 0,
    delivery: 0
  };
  status = [
    {
      status: 0,
      type: 'Pendiente'
    },
    {
      status: 1,
      type: 'En tráfico'
    },
    {
      status: 2,
      type: 'Entregado'
    }
  ];
  constructor(private cartservices: CartService) { }

  ngOnInit() {
    this.getMyOrders();
  }
  getMyOrders() {
    this.cartservices.getAllOrders()
    .subscribe(
      data => { console.log(data); this.myOrders = data; 
        for(let ord = 0; ord <this.myOrders.length; ord++) {
          this.myOrders[ord].ordertotal = 0;
          for(let i = 0; i < this.myOrders[ord].products.length; i++) {
            let prodTotal = 0;
            let countnumber = this.myOrders[ord].products[i].minunites;
            this.myOrders[ord].products[i].count = [];
            for ( let prod = this.myOrders[ord].products[i].minunites; prod < this.myOrders[ord].products[i].maxunites+1; prod++ ) {
              if (prod % this.myOrders[ord].products[i].units_pack === 0) {
                this.myOrders[ord].products[i].count.push(countnumber);
              }
              countnumber = countnumber + 1;
            }
            if (this.myOrders[ord].products[i].ismultiple){
              prodTotal = (this.myOrders[ord].products[i].price * this.myOrders[ord].products[i].quantity) * this.myOrders[ord].products[i].multiplicate;
            } else {
              prodTotal = this.myOrders[ord].products[i].price * this.myOrders[ord].products[i].quantity;
            }
            this.myOrders[ord].ordertotal = this.myOrders[ord].ordertotal + prodTotal;
            
          }
          this.myOrders[ord].ordertotal =  this.myOrders[ord].ordertotal +  this.myOrders[ord].delivery;
          console.log( this.myOrders[ord]);
        }
      },
      err => console.error(err),
      () => console.log('done loading image')
    );
  }
  updateOrder(stat, id) {
    const state = {
      status: stat
    };
    this.cartservices.updateOrder(id, state).subscribe(
      data => { console.log(data); this.getMyOrders(); this.curOrder ={
        status: 0,
        buyer: {
          phones: []
        },
        products:[],
        ordertotal: 0,
        delivery: 0
      }; },
      err => console.error(err),
      () => console.log('done loading image')
    );
  }
  
  orderDetail(order) {
    console.log(order);
    this.curOrder = {
      status: 0,
      buyer: {
        phones: []
      },
      products:[],
      ordertotal: 0,
      delivery: 0
    };
    this.curOrder.ordertotal = 0;
    this.curOrder = order;
    console.log(this.curOrder);

    UIkit.modal('#ordermodal').show();
  }

  getcurrentCart() {
    this.curOrder.ordertotal = 0;
    for(let i = 0; i < this.curOrder.products.length; i++) {
      let prodTotal = 0;
      let countnumber = this.curOrder.products[i].minunites;
      this.curOrder.products[i].count = [];
      for ( let prod = this.curOrder.products[i].minunites; prod < this.curOrder.products[i].maxunites+1; prod++ ) {
        if (prod % this.curOrder.products[i].units_pack === 0) {
          this.curOrder.products[i].count.push(countnumber);
        }
        countnumber = countnumber + 1;
      }
      if (this.curOrder.products[i].ismultiple){
        prodTotal = (this.curOrder.products[i].price * this.curOrder.products[i].quantity) * this.curOrder.products[i].multiplicate;
      } else {
        prodTotal = this.curOrder.products[i].price * this.curOrder.products[i].quantity;
      }
      this.curOrder.ordertotal = this.curOrder.ordertotal + prodTotal;
      console.log( this.curOrder);
      
    }
    this.curOrder.ordertotal = this.curOrder.ordertotal + this.curOrder.delivery;
  }
  updateOrderLive(order) {
    order.ordertotal = 0;
    for(let i = 0; i < order.products.length; i++) {
      let prodTotal = 0;
      let countnumber = order.products[i].minunites;
      order.products[i].count = [];
      for ( let prod = order.products[i].minunites; prod < order.products[i].maxunites+1; prod++ ) {
        if (prod % order.products[i].units_pack === 0) {
          order.products[i].count.push(countnumber);
        }
        countnumber = countnumber + 1;
      }
      if (order.products[i].ismultiple){
        prodTotal = (order.products[i].price * order.products[i].quantity) * order.products[i].multiplicate;
      } else {
        prodTotal = order.products[i].price * order.products[i].quantity;
      }
      order.ordertotal = order.ordertotal + prodTotal;
      console.log( order);
      
    }
    
    this.cartservices.updateOrder(order._id, order).subscribe(
      data => {
        UIkit.notification({message: '<span uk-icon=\'icon: check\'></span> Actualizaste la orden exitosamente!', pos: 'top-right'});
        this.getcurrentCart();
        this.curOrder = {
          status: 0,
          buyer: {
            phones: []
          },
          products:[],
          ordertotal: 0,
          delivery: 0
        };
        UIkit.modal('#ordermodal').hide();
      },
      err => console.error(err),
      () => console.log('done loading image')
    );
}

  
}
