export class Product {

    id: string;
    title: string;
    price: number;
    photo: string;
    mindelivery: number;
    maxdelivery: number;
    minunites: number;
    maxunites: number;
    quantity: number;
    deliverydate: string;
    units_pack: number;
    type_unit: string;
    multiplicate: number;
    ismultiple: boolean;
}