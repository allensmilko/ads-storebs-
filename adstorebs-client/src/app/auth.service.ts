import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { GlobalVariable } from "./config";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private http: HttpClient) {}

  register(data) {
    return this.http.post(GlobalVariable.BASE_API_URL + "auth/register", data);
  }
  login(data) {
    return this.http.post(GlobalVariable.BASE_API_URL + "auth/login", data);
  }
  updateuser(data, id) {
    return this.http.post(
      GlobalVariable.BASE_API_URL + "auth/update-user/" + id,
      data
    );
  }
  activateAccount(data, id) {
    return this.http.post(
      GlobalVariable.BASE_API_URL + "auth-activate-account/" + id,
      data
    );
  }
  getusers() {
    return this.http.get(GlobalVariable.BASE_API_URL + "auth/getusers");
  }
  forgotPAss(email) {
    return this.http.get(
      GlobalVariable.BASE_API_URL + "auth/generate-pass/" + email
    );
  }
  removeuser(id) {
    return this.http.get(
      GlobalVariable.BASE_API_URL + "auth/remove-user/" + id
    );
  }
  getToken() {
    return localStorage.getItem("adstore-auth-token");
  }
  getMyAccount() {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append("Authorization", this.getToken());
    return this.http.get(GlobalVariable.BASE_API_URL + "auth/me", {
      headers: currentHeaders,
    });
  }
  setAvatar(data) {
    const form = new FormData();
    form.append("image", data);
    const headers = new HttpHeaders();
    const currentHeaders = headers.append("Authorization", this.getToken());
    const options = { headers: currentHeaders };
    return this.http.post(
      GlobalVariable.BASE_API_URL + "user/set-avatar",
      form,
      options
    );
  }
}
