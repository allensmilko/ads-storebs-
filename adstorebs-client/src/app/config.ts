export const GlobalVariable = Object.freeze({
  // BASE_API_URL: "http://localhost:3000/api/v1/",
  BASE_API_URL:
    "https://64fnl68o7a.execute-api.us-east-1.amazonaws.com/prod/api/v1/",
  TOKEN: localStorage.getItem("adstore-auth-token"),
  IMAGESURI: "https://files-adstore.sfo2.digitaloceanspaces.com/",
});
