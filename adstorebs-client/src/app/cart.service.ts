import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from './models/product';
import { GlobalVariable } from './config';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private products: Product[];
  private product = {
    id: '',
    title: '',
    price: 0,
    photo: '',
    mindelivery: 0,
    maxdelivery: 0,
    minunites: 0,
    maxunites: 0,
    quantity: 0,
    deliverydate: ''
  };
  constructor(private http: HttpClient) {
      this.products = [ ];
      console.log(localStorage.getItem('ads-current-cart'));
      if (localStorage.getItem('ads-current-cart') !== null && localStorage.getItem('ads-current-cart').length > 0) {
          this.products = JSON.parse(localStorage.getItem('ads-current-cart'));
          console.log('::::_____-');
          console.log(this.products);
      }
  }

  findAll(): Product[] {
        return this.products;
  }

  findById(id: string) {
    for (let i = 0; i < this.products.length; i++) {
        if (this.products[i].id === id) {
            return this.products[i];
        }
    }
    return -1;
  }
  getQuantity(id: string) {
    for (let i = 0; i < this.products.length; i++) {
        if (this.products[i].id === id) {
            return this.products[i].quantity;
        }
    }
    return -1;
  }
  getMultiplication(id: string) {
    for (let i = 0; i < this.products.length; i++) {
        if (this.products[i].id === id) {
            return this.products[i].multiplicate;
        }
    }
    return -1;
  }
  getToken() {
    return localStorage.getItem('adstore-auth-token');
  }
  addToCart(product) {
        const prod =  new Product();
        prod.id = product._id;
        prod.title = product.title;
        prod.price = product.price;
        prod.photo = product.images[0].url;
        prod.mindelivery = product.min_delivery;
        prod.maxdelivery = product.max_delivery;
        prod.minunites = product.min_unites;
        prod.maxunites = product.max_unites;
        prod.quantity = product.quantity;
        prod.type_unit = product.type_unit;
        prod.units_pack = product.units_pack;
        prod.multiplicate = product.multiplicate;
        prod.ismultiple = product.ismultiple;
        console.log(product._id);
        if (!this.getSelectedIndex(product._id)) {
            console.log('Lo es');
            this.findAll().push(prod);
            return  this.saveCart();
        } else {
            console.log('No Lo es');

            for (let i = 0; i < this.products.length; i++) {
                if (this.products[i].id === product._id) {
                    this.products[i] = prod;
                    return  this.saveCart();
                }
            }
        }

        this.product = {
            id: '',
            title: '',
            price: 0,
            photo: '',
            mindelivery: 0,
            maxdelivery: 0,
            minunites: 0,
            maxunites: 0,
            quantity: 0,
            deliverydate: ''
          };
    }
    updateCart(prod) {
      for (let i = 0; i < this.products.length; i++) {
        if (this.products[i].id === prod.id) {
            this.products[i] = prod;
            return  this.saveCart();
        }
    }

      this.product = {
          id: '',
          title: '',
          price: 0,
          photo: '',
          mindelivery: 0,
          maxdelivery: 0,
          minunites: 0,
          maxunites: 0,
          quantity: 0,
          deliverydate: ''
        };
  }

    saveCart() {
        console.log(this.findAll());
        return localStorage.setItem('ads-current-cart', JSON.stringify(this.products));
    }
    removeItem(id: string) {
        for (let i = 0; i < this.products.length; i++) {
            if (this.products[i].id === id) {
                 this.products.splice(i, 1);
                 return  this.saveCart();
            }
        }
    }
    emptyCart() {
      this.products = [];
      return  this.saveCart();
    }
    getSelectedIndex(id: string) {
      for (let i = 0; i < this.products.length; i++) {
          if (this.products[i].id === id) {
              return true;
          }
      }
      return false;
  }
  shopNow(order) {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', this.getToken());
    return this.http.post(GlobalVariable.BASE_API_URL + 'orders/new-order', order, {headers: currentHeaders });
  }
  getLatestOrder() {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', this.getToken());
    return this.http.get(GlobalVariable.BASE_API_URL + 'orders/get-latest-order',  {headers: currentHeaders });
  }
  getMyOrders() {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', this.getToken());
    return this.http.get(GlobalVariable.BASE_API_URL + 'orders/get-user-orders', {headers: currentHeaders });
  }
  getAllOrders() {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', this.getToken());
    return this.http.get(GlobalVariable.BASE_API_URL + 'orders/get-all-orders', {headers: currentHeaders });
  }
  updateOrder(id, data) {
    return this.http.post(GlobalVariable.BASE_API_URL + 'orders/update-order/' + id, data);
  }
}
