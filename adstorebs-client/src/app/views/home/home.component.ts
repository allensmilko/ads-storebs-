import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../products.service';
import { ActivatedRoute, Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  products: any = {
    publications: [],
    count: 0
  };
  categoria = '';
  categories: any = [];
  curcat = '';
  constructor(private serviceProduct: ProductsService, private route: ActivatedRoute, private router: Router) {
    router.events.subscribe( (event: Event) => {

      if (event instanceof NavigationStart) {
          // Show loading indicator
      }

      if (event instanceof NavigationEnd) {
          // Hide loading indicator
          this.getProducts();
      }

      if (event instanceof NavigationError) {
          // Hide loading indicator

          // Present error to user
          console.log(event.error);
      }
    });
  }
  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    let datos = {};
    const currentUrl = this.router.url;
    if (currentUrl !== '/') {
      datos = {
        category: this.route.snapshot.paramMap.get('categoria')
      };
      this.getCategories(this.route.snapshot.paramMap.get('categoria'));
      this.categoria = this.route.snapshot.paramMap.get('categoria');
    }
    this.serviceProduct.getPublications(datos, true)
    .subscribe(
      data => { console.log(data); this.products = data; },
      err => console.error(err),
      () => console.log('done loading foods')
    );

  }
  getCategories(id) {
    this.serviceProduct.getCategories()
    .subscribe(
      data => { console.log(data);
                this.categories = data;
                for( var i = 0; i < this.categories.length; i++ ){
                  if ( this.categories[i]._id === id ) {
                    this.curcat = this.categories[i].name;
                    console.log(this.curcat);
                  }
                }
         },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
}
