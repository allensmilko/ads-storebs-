import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { GlobalVariable } from '../../config';

declare var UIkit: any;

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  user: any = {
    _id: '',
    isfirsttime: true,
    avatar: ''
  };
  prodata: any = {
    avatar: ''
  };
  globaluri = GlobalVariable.IMAGESURI;
  constructor(private authservice: AuthService, private router: Router) { }

  ngOnInit() {
    this.getMyData();
  }
  getMyData() {
    this.authservice.getMyAccount().subscribe(
      resp => {
        console.log(resp);
        this.user = resp;
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
  updateuser() {
    this.user.isfirsttime = false;
    this.authservice.updateuser(this.user, this.user._id).subscribe(
      resp => {
        console.log(resp);
        this.user = resp;
        UIkit.modal.alert('Actualización realizada con exito!');
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      console.log(fileInput.target.files[0]);
      this.authservice.setAvatar(fileInput.target.files[0])
      .subscribe(
        // tslint:disable-next-line:max-line-length
        data => {
          console.log(data);
          this.prodata = data;
          this.user.avatar = this.prodata.avatar;
          this.prodata = {
            avatar: ''
          };
        },
        err => console.error(err),
        () => console.log('done loading image')
      );
  }
}
}
