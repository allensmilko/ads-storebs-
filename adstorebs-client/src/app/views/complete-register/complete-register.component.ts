import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { GlobalVariable } from '../../config';

@Component({
  selector: 'app-complete-register',
  templateUrl: './complete-register.component.html',
  styleUrls: ['./complete-register.component.scss']
})
export class CompleteRegisterComponent implements OnInit {
  user: any = {
    _id: '',
    isfirsttime: true,
    avatar: '',
    phones:[0,0]
  };
  prodata: any = {
    avatar: ''
  };
  
  globaluri = GlobalVariable.IMAGESURI;
  constructor(private authservice: AuthService, private router: Router) { }

  ngOnInit() {
    console.log(this.user);
    this.getMyData();
  }
  getMyData() {
    this.authservice.getMyAccount().subscribe(
      resp => {
        this.user = resp;
        console.log(this.user);

      },
      err => console.error(err),
      () => console.log('done')
    );
  }
  updateuser() {
    this.user.isfirsttime = false;
    this.authservice.updateuser(this.user, this.user._id).subscribe(
      resp => {
        console.log(resp);
        this.user = resp;
        this.router.navigate(['']);
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      console.log(fileInput.target.files[0]);
      this.authservice.setAvatar(fileInput.target.files[0])
      .subscribe(
        // tslint:disable-next-line:max-line-length
        data => {
          console.log(data);
          this.prodata = data;
          this.user.avatar = this.prodata.avatar;
          this.prodata = {
            avatar: ''
          };
        },
        err => console.error(err),
        () => console.log('done loading image')
      );
  }
}

}
