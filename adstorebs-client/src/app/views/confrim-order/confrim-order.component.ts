import { Component, OnInit } from '@angular/core';
import { CartService } from '../../cart.service';
import { GlobalVariable } from '../../config';
import { AuthService } from '../../auth.service';
import { Router } from "@angular/router";
declare var UIkit: any;

@Component({
  selector: 'app-confrim-order',
  templateUrl: './confrim-order.component.html',
  styleUrls: ['./confrim-order.component.scss']
})
export class ConfrimOrderComponent implements OnInit {
  products: any = [];
  isloading = true;
  globaluri = GlobalVariable.IMAGESURI;
  totalCart = 0;
  today: number = Date.now();
  userData: any = {
    _id: '',
    coins: 0
  };
  resp: any = {
    _id: '',
    status: 0,
    code: 0
  };
  order: any = {
    order: 0
  };
  constructor(private auth: AuthService, private cartServ: CartService, private router: Router) { }

  ngOnInit() {
    this.getCart();
  }
  getCart() {
    this.products = this.cartServ.findAll();
    console.log(this.products);
    this.totalCart = 0;
    for(let i = 0; i < this.products.length; i++) {
      let prodTotal = 0;
      if (this.products[i].ismultiple){
        prodTotal = (this.products[i].price * this.products[i].quantity) * this.products[i].multiplicate;
      } else {
        prodTotal = this.products[i].price * this.products[i].quantity;
      }
      this.totalCart = this.totalCart + prodTotal;
    }
    this.getMyData();
    this.isloading = false;
  }
  removeItem(id) {
    console.log(id);
    this.cartServ.removeItem(id);
    this.getCart();
  }
  shopNow() {
    this.isloading = true;
    if (this.totalCart > this.userData.coins) {
      UIkit.modal.alert('No tienes saldo suficiente para realizar esta compra');
    } else {
      const data = {
        ordertotal: this.totalCart,
        products: this.products
      };
      this.cartServ.shopNow(data)
      .subscribe(
        response => {
          console.log(response);
          this.resp = response;
          if (this.resp._id.length) {
            console.log('creado');
            UIkit.modal('#order-success').show();
          }
        },
        err => {
          this.isloading = false;

          console.error(err)
        },
        () => console.log('done loading foods')
      );
    }
  }
  closeAndComeBack() {
    UIkit.modal('#order-success').hide();
    this.cartServ.emptyCart();
    this.upDatUs();
  }
  getMyData() {
    this.cartServ.getLatestOrder()
    .subscribe(
      resp => {
        this.order = resp;
      },
      err => console.error(err),
      () => console.log('done')
    );

    this.auth.getMyAccount()
    .subscribe(
      resp => {
        console.log(resp);
        this.userData = resp;
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
  upDatUs() {
    this.auth.getMyAccount()
    .subscribe(
      resp => {
        console.log(resp);
        this.userData = resp;
        this.userData.coins = this.userData.coins - this.totalCart;
        this.auth.updateuser(this.userData, this.userData._id)
        .subscribe(
          data => {
            console.log(data);
            this.router.navigate(['/']).then(() => {
            location.reload();
            });
          },
          err => console.error(err),
          () => console.log('done loading foods')
        );
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
}
