import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tc',
  templateUrl: './tc.component.html',
  styleUrls: ['./tc.component.scss']
})
export class TcComponent implements OnInit {
  pdfSrc: string = '/assets/tc.pdf';
  constructor() { }

  ngOnInit() {
  }

}
