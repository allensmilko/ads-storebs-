import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
declare var UIkit: any;

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  user = {
    country: -1,
    rol: 2,
    email: ''
  };
  response: any = {
    code: 0,
    token: '',
    rol: 0
  };
  curcountry: '';
  showCountriesList = false;
  uri = '../../../../assets/countries/';
  countries = [
    {
      id: 0,
      name: 'Panamà',
      flag: 'panama.png'
    },
    {
      id: 1,
      name: 'Costa rica',
      flag: 'costarica.png'
    },
    {
      id: 2,
      name: 'Honduras',
      flag: 'honduras.png'
    },
    {
      id: 3,
      name: 'Guatemala',
      flag: 'guatemala.png'
    },
    {
      id: 4,
      name: 'Puerto Rico',
      flag: 'puertorico.png'
    },
    {
      id: 6,
      name: 'Cayman Islands',
      flag: 'caymanislands.png'
    },
    {
      id: 7,
      name: 'Jamaica',
      flag: 'jamaica.png'
    },
    {
      id: 8,
      name: 'Bahamas',
      flag: 'bahamas.png'
    },
    {
      id: 9,
      name: 'Aruba',
      flag: 'aruba.png'
    },
    {
      id: 10,
      name: 'República dominicana',
      flag: 'republicadominicana.png'
    },
    {
      id: 11,
      name: 'Nicaragua',
      flag: 'nicaragua.png'
    },
    {
      id: 12,
      name: 'El salvador',
      flag: 'el-salvador.png'
    }
  ];
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  register() {
    this.auth.register(this.user)
    .subscribe(
      data => { console.log(data);
                this.response = data;
                if (this.response.code === 100) {
                  UIkit.modal.alert('Te registraste conexito! , pronto te enviaremos un E-mail para continuar el proceso.')
                      .then(() => {
                        location.reload(true);
                      }, () => {
                        location.reload(true);
                      });
                } else if (this.response.code === 404) {
                  UIkit.modal.alert('Este usuario ya existe');
                } else {
                  UIkit.modal.alert('Verifica que los campos esten llenos de forma correcta');
                }
      },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  login() {
    this.auth.login(this.user)
    .subscribe(
      data => { console.log(data);
                this.response = data;
                if (this.response.code === 100) {
                  localStorage.setItem('adstore-auth-token', this.response.token);
                  if (this.response.rol === 2) {
                    this.router.navigate(['/']);
                  } else{
                    this.router.navigate(['/admin/list']);
                  }
                } else if (this.response.code === 401) {
                  UIkit.modal.alert('El usuario no ha sido activado');
                } else {
                  UIkit.modal.alert('Verifica que los campos esten llenos de forma correcta');
                }
      },
      err => console.error(err),
      () => console.log('done loading foods')
    );
      //   UIkit.modal.alert('UIkit alert!');


  }
  forgotPAss() {
    this.auth.forgotPAss(this.user.email)
    .subscribe(
      data => { console.log(data);
                this.response = data;
                UIkit.modal('#modal-forgot-pass').hide();
                if (this.response.code === 404) {
                  UIkit.modal.alert('Este usuario no esta registrado.');
                } else {
                  UIkit.modal.alert('Generamos una contraseña nueva para tu cuenta, ahora revisa tu correo para iniciar sesión.');
                }
      },
      err => {
        UIkit.modal('#modal-forgot-pass').hide();
        UIkit.modal.alert('Este usuario no esta registrado.');
      },
      () => console.log('done loading foods')
    );
  }
}
