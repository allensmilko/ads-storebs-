import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { ProductsComponent } from './admin/views/products/products.component';
import { EditProductComponent } from './admin/views/edit-product/edit-product.component';
import { ProductDetailComponent } from './global/product-detail/product-detail.component';
import { CartComponent } from './global/cart/cart.component';
import { AuthComponent } from './views/auth/auth.component';
import { UsersComponent } from './admin/views/users/users.component';
import { CompleteRegisterComponent } from './views/complete-register/complete-register.component';
import { AccountComponent } from './views/account/account.component';
import { ConfrimOrderComponent } from './views/confrim-order/confrim-order.component';
import { CategoriesComponent } from './admin/views/categories/categories.component';
import { OrdersComponent } from './admin/views/orders/orders.component';
import { MyOrdersComponent } from './global/my-orders/my-orders.component';
import { TcComponent } from './views/tc/tc.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'account', component: AccountComponent },
  { path: 'complete-register', component:  CompleteRegisterComponent },
  { path: 'carrito-de-compras', component: CartComponent },
  { path: 'confirmar-order', component: ConfrimOrderComponent },
  { path: 'filtro/:categoria', component: HomeComponent },
  { path: 'detalle-producto/:id', component: ProductDetailComponent },
  { path: 'my-orders', component: MyOrdersComponent },
  {
    path: 'terminos-y-condiciones',
    component: TcComponent
  },
  { path: 'admin',
  children: [
    {
    path:  'list',
    component:  ProductsComponent
    },
    {
      path: 'gestion-publicacion/:id',
      component: EditProductComponent
    },
    {
      path: 'usuarios',
      component: UsersComponent
    },
    {
      path: 'categorias',
      component: CategoriesComponent
    },
    {
      path: 'ordenes',
      component: OrdersComponent
    }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
