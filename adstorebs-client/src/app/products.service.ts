import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from './config';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  newPublication(token, data) {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', token);
    const options = { headers: currentHeaders };
    return this.http.post(GlobalVariable.BASE_API_URL + 'publications/new-publication',  options);
  }
  updatePublication(token, publicationId, data) {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', token);
    const options = { headers: currentHeaders };
    return this.http.post(GlobalVariable.BASE_API_URL + 'publications/update-publication/' + publicationId, data );
  }
  addImageToPublication(token, publicationId, data) {
    const form = new FormData();
    form.append('image', data);
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', token);
    const options = { headers: currentHeaders };
    return this.http.post(GlobalVariable.BASE_API_URL + 'publications/add-image/' + publicationId, form );
  }
  getPublications(data, published) {
      return this.http.post(GlobalVariable.BASE_API_URL + 'publications/get-publications/' + published, data);
  }
  getMyPublications(token) {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', token);
    console.log(token);
    return this.http.get(GlobalVariable.BASE_API_URL + 'publications/my-publications', {headers: currentHeaders });
  }
  getPublicationIdPrivated(token, id) {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', token);
    console.log(token);
    return this.http.get(GlobalVariable.BASE_API_URL + 'publications/publication-detail/' + id );
  }
  removeProduct(id) {
    return this.http.get(GlobalVariable.BASE_API_URL + 'publications/remove-publication/' + id );
  }
  removeImage(id) {
    return this.http.get(GlobalVariable.BASE_API_URL + 'publications/remove-image/' + id );
  }
  newCategory(token, data) {
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', token);
    const options = { headers: currentHeaders };
    return this.http.post(GlobalVariable.BASE_API_URL + 'publications/categories/new-category',  data);
  }
  getCategories() {
    return this.http.get(GlobalVariable.BASE_API_URL + 'publications/categories/get-categories');
  }
  removeCategory(categoryId) {
    return this.http.get(GlobalVariable.BASE_API_URL + 'publications/categories/remove-category/' + categoryId);
  }
  addImageToCategory(token, categoryId, data) {
    const form = new FormData();
    form.append('image', data);
    const headers = new HttpHeaders();
    const currentHeaders = headers.append('Authorization', token);
    const options = { headers: currentHeaders };
    return this.http.post(GlobalVariable.BASE_API_URL + 'publications/categories/add-category-image/' + categoryId, form );
  }
}
