import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './global/header/header.component';
import { ButtonComponent } from './global/button/button.component';
import { SidebarComponent } from './global/sidebar/sidebar.component';
import { ProductCardComponent } from './global/product-card/product-card.component';
import { SearchBarComponent } from './global/search-bar/search-bar.component';
import { PaginatorComponent } from './global/paginator/paginator.component';
import { HomeComponent } from './views/home/home.component';
import { ProductsListComponent } from './global/products-list/products-list.component';
import { ProductsComponent } from './admin/views/products/products.component';
import { EditProductComponent } from './admin/views/edit-product/edit-product.component';
import { ProductDetailComponent } from './global/product-detail/product-detail.component';
import { AuthComponent } from './views/auth/auth.component';
import { CartComponent } from './global/cart/cart.component';
import { UsersComponent } from './admin/views/users/users.component';
import { CompleteRegisterComponent } from './views/complete-register/complete-register.component';
import { AccountComponent } from './views/account/account.component';
import { ConfrimOrderComponent } from './views/confrim-order/confrim-order.component';
import { CategoriesComponent } from './admin/views/categories/categories.component';
import { OrdersComponent } from './admin/views/orders/orders.component';
import { MyOrdersComponent } from './global/my-orders/my-orders.component';
import { TcComponent } from './views/tc/tc.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ButtonComponent,
    SidebarComponent,
    ProductCardComponent,
    SearchBarComponent,
    PaginatorComponent,
    HomeComponent,
    ProductsListComponent,
    ProductsComponent,
    EditProductComponent,
    ProductDetailComponent,
    AuthComponent,
    CartComponent,
    UsersComponent,
    CompleteRegisterComponent,
    AccountComponent,
    ConfrimOrderComponent,
    CategoriesComponent,
    OrdersComponent,
    MyOrdersComponent,
    TcComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CurrencyMaskModule,
    PdfViewerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
