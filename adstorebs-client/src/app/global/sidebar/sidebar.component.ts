import { Component, OnInit, Input } from '@angular/core';
import { ProductsService } from '../../products.service';
import { GlobalVariable } from '../../config';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() isadmin = false;
  @Input() categoria = '';
  globaluri = GlobalVariable.IMAGESURI;
  category = {
    name: ''
  };
  categories: any = [];

  constructor(private serviceRpoducts: ProductsService) { }

  ngOnInit() {
    console.log('_::::::::.');
    console.log(this.categoria);
    this.getCategories();
  }
  newCategory() {
    this.serviceRpoducts.newCategory('token', this.category)
    .subscribe(
      data => { console.log(data); this.getCategories(); },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  getCategories() {
    this.serviceRpoducts.getCategories()
    .subscribe(
      data => { console.log(data); this.categories = data; },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  onFileChange(fileInput: any, categoryId) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      console.log(fileInput.target.files[0]);
      this.serviceRpoducts.addImageToCategory('', categoryId, fileInput.target.files[0])
      .subscribe(
        // tslint:disable-next-line:max-line-length
        data => { console.log(data); this.getCategories(); },
        err => console.error(err),
        () => console.log('done loading image')
      );
  }
}
}
