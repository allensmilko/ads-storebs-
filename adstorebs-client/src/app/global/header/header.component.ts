import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../../cart.service';
import { AuthService } from '../../auth.service';
import { GlobalVariable } from '../../config';

declare var UIkit: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userData: any = {
    isfirsttime: false
  };
  globalUri = GlobalVariable.IMAGESURI;
  constructor(private cartService: CartService, private authservice: AuthService, private router: Router) { }

  ngOnInit() {
    // this.showAlert();
    this.getMyData();
  }
  // showAlert(): void {
  //   UIkit.modal.alert('UIkit alert!');
  // }
  cerrarSesion() {
    localStorage.clear();
    location.reload();
  }
  getMyData() {
    this.authservice.getMyAccount()
    .subscribe(
      resp => {
        console.log(resp);
        this.userData = resp;
        if (this.userData.isfirsttime) {
          console.log('NO LO ES')
          this.router.navigate(['complete-register']);
        }
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
}
