import { Component, OnInit, Input } from '@angular/core';
import { CartService } from '../../cart.service';
import { Product } from '../../models/product';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  private products: Product[];
  userData = {};
  totalcart: number;
  constructor(private cartService: CartService, private authservice: AuthService) {
  }

  ngOnInit() {
    this.products = this.cartService.findAll();
    this.totalcart = this.cartService.findAll().length;
    this.getMyData();
  }
  getMyData() {
    this.authservice.getMyAccount()
    .subscribe(
      resp => {
        console.log(resp);
        this.userData = resp;
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
}
