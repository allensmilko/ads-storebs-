import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../products.service';
import { ActivatedRoute } from '@angular/router';
import { GlobalVariable } from '../../config';
import { CartService } from '../../cart.service';

declare var UIkit: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  id: string;
  uri: string;
  count: any = [];
  countnumber = 0;
  added = false;
  forUpdate = false;
  multiple = [];
  product: any = {
    images: [],
    min_unites: 0,
    max_unites: 0,
    quantity: 0,
    multiplicate: 0,
    units_pack: 0,
    type_unit: ''
  };
  constructor(private productService: ProductsService, private route: ActivatedRoute, private cartServices: CartService) { 
   this.id = this.route.snapshot.paramMap.get('id');
   this.uri = GlobalVariable.IMAGESURI;
  }

  ngOnInit() {
    this.getProductInfo();
  }
  getProductInfo() {
    this.productService.getPublicationIdPrivated('', this.id)
    .subscribe(
      data => {
        console.log(data);
        this.product = data;
        this.countnumber = this.product.min_unites;
        for ( let i = this.product.min_unites; i < this.product.max_unites+1; i++ ) {
          if (i % this.product.units_pack === 0) {
            this.count.push(this.countnumber);
          }
          this.countnumber = this.countnumber + 1;
        }
        this.product.quantity = this.count[0];
        for ( let i = 0; i < 50 ; i ++) {
          this.multiple.push(i);
        }
        console.log(this.multiple);
        console.log(this.product.quantity);
        console.log(this.cartServices.getSelectedIndex(this.id));
        if (this.cartServices.getSelectedIndex(this.id)) {
          this.added = true;
          console.log(this.cartServices.getQuantity(this.id));
          this.product.quantity = this.cartServices.getQuantity(this.id);
          this.product.multiplicate = this.cartServices.getMultiplication(this.id);
        }
       },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  setIndex(index) {
    UIkit.slider('#ads-product-sliner').show(index);

  }
  addTocat() {
    console.log(this.product);
    this.cartServices.addToCart(this.product);
    this.getProductInfo();
    const mensaje = 'Agregaste "' + this.product.title + '" con ' + this.product.quantity + ' unidades exitosamente!';
    UIkit.notification({message: '<span uk-icon=\'icon: check\'></span>' + mensaje, pos: 'top-right'});
  }
}
