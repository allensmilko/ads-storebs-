import { Component, OnInit } from '@angular/core';
import { CartService } from '../../cart.service';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent implements OnInit {
  myOrders: any = {};
  status = [
    {
      status: 0,
      type: 'Pendiente'
    },
    {
      status: 1,
      type: 'En tráfico'
    },
    {
      status: 2,
      type: 'Entregado'
    }
  ];
  constructor(private cartservices: CartService) { }

  ngOnInit() {
    this.getMyOrders();
  }
  getMyOrders() {
    this.cartservices.getMyOrders()
    .subscribe(
      data => {
         console.log(data); 
         this.myOrders = data; 
        for(let ord = 0; ord <this.myOrders.orders.length; ord++) {
          this.myOrders.orders[ord].ordertotal = 0;
          for(let i = 0; i < this.myOrders.orders[ord].products.length; i++) {
            let prodTotal = 0;
            let countnumber = this.myOrders.orders[ord].products[i].minunites;
            this.myOrders.orders[ord].products[i].count = [];
            for ( let prod = this.myOrders.orders[ord].products[i].minunites; prod < this.myOrders.orders[ord].products[i].maxunites+1; prod++ ) {
              if (prod % this.myOrders.orders[ord].products[i].units_pack === 0) {
                this.myOrders.orders[ord].products[i].count.push(countnumber);
              }
              countnumber = countnumber + 1;
            }
            if (this.myOrders.orders[ord].products[i].ismultiple){
              prodTotal = (this.myOrders.orders[ord].products[i].price * this.myOrders.orders[ord].products[i].quantity) * this.myOrders.orders[ord].products[i].multiplicate;
            } else {
              prodTotal = this.myOrders.orders[ord].products[i].price * this.myOrders.orders[ord].products[i].quantity;
            }
            this.myOrders.orders[ord].ordertotal = this.myOrders.orders[ord].ordertotal + prodTotal;
            
          }
          this.myOrders.orders[ord].ordertotal =  this.myOrders.orders[ord].ordertotal +  this.myOrders.orders[ord].delivery;
          console.log( this.myOrders.orders[ord]);
        }
      },
      err => console.error(err),
      () => console.log('done loading image')
    );
  }

}
