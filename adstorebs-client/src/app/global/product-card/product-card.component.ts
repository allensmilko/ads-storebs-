import { Component, OnInit, Input } from '@angular/core';
import { GlobalVariable } from '../../config';
import { ProductsService } from '../../products.service';
import { CartService } from '../../cart.service';
declare var UIkit: any;

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input() product: any = {
    min_unites: 0,
    quantity: 0
  };
  @Input() goto = '';
  @Input() isadmin = false;
  globaluri = GlobalVariable.IMAGESURI;

  constructor(private serviceProduct: ProductsService, private cartServices: CartService) { }

  ngOnInit() {
  }
  remove(id) {
    this.serviceProduct.removeProduct(id)
    .subscribe(
      resp => {
        location.reload();
      },
      err => console.error(err),
      () => console.log('done')
    );
  }
  addTocat(product) {
    product.quantity = product.min_unites;
    this.cartServices.addToCart(product);
    const mensaje = 'Agregaste "' + product.title + '" con ' + product.quantity + ' unidades exitosamente!';
    UIkit.notification({message: '<span uk-icon=\'icon: check\'></span>' + mensaje, pos: 'top-right'});
  }
}
