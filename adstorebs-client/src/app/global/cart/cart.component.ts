import { Component, OnInit } from '@angular/core';
import { CartService } from '../../cart.service';
import { GlobalVariable } from '../../config';
declare var UIkit: any;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  products: any = [];
  globaluri = GlobalVariable.IMAGESURI;
  totalCart = 0;
  constructor(private cartServ: CartService) { }

  ngOnInit() {
    this.getCart();
  }
  getCart() {
    this.products = this.cartServ.findAll();
    console.log(this.products);
    this.totalCart = 0;
    for(let i = 0; i < this.products.length; i++) {
      let prodTotal = 0;
      let countnumber = this.products[i].minunites;
      this.products[i].count = [];
      for ( let prod = this.products[i].minunites; prod < this.products[i].maxunites+1; prod++ ) {
        if (prod % this.products[i].units_pack === 0) {
          this.products[i].count.push(countnumber);
        }
        countnumber = countnumber + 1;
      }
      console.log( this.products);
      if (this.products[i].ismultiple){
        prodTotal = (this.products[i].price * this.products[i].quantity) * this.products[i].multiplicate;
      } else {
        prodTotal = this.products[i].price * this.products[i].quantity;
      }
      
      this.totalCart = this.totalCart + prodTotal;
    }
    
  }
  updateProduct(product) {
    console.log(product);
    this.cartServ.updateCart(product);
    const mensaje = 'Actualizaste "' + product.title + '" con ' + product.quantity + ' unidades exitosamente!';
    UIkit.notification({message: '<span uk-icon=\'icon: check\'></span>' + mensaje, pos: 'top-right'});
    this.getCart();
  }
  removeItem(id) {
    console.log(id);
    this.cartServ.removeItem(id);
    this.getCart();
  }
}
